package platformer.view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import platformer.Main;
import platformer.model.*;
import platformer.model.Character;
import platformer.model.Stage;

import java.util.ArrayList;

import platformer.model.Clock;

public class Platform extends Pane {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    //    default =300
    public static final int GROUND = 360;
    private Score score;
    private Score2 score2;
    private Image platformImg;
    private Character mainCharacter;
    private Character2 mainCharacter2;
    private ArrayList<Score> scoreList;
    private ArrayList<Enemy> enemyList;
    private Keys keys;
    private ArrayList<Stage> stageList;

    private Image platformImg2;
    private MediaPlayer player;
    private MediaView mediaView;
    ImageView backgroundImg2;

    public static Button startBtn;
    public static Button exitBtn;
    public static Button retry;
    public static Button exit2;

    private Image startPic = new Image(getClass().getResourceAsStream("/platformer/assets/start.png"), 150, 62.5, false, false);
    private Image exitPic = new Image(getClass().getResourceAsStream("/platformer/assets/exit.png"), 150, 62.5, false, false);


    public Platform() {
        MediaPlayer mediaPlayer;
        enemyList = new ArrayList();

//        Media musicFile = new Media("file:///C:/Users/MSI-PC/Desktop/ops/JUMPPART/src/platformer/assets/SuperMario.mp3");
        //media added
        Media musicFile = new Media("file:///F:/SE33_5509/final_Assignment_lasters_ver/Final/src/platformer/assets/SuperMario.mp3");

        mediaPlayer = new MediaPlayer(musicFile);
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setVolume(0.2);

        keys = new Keys();
//        platformImg = new Image(getClass().getResourceAsStream("/platformer/assets/Background.png"));
        platformImg = new Image(getClass().getResourceAsStream("/platformer/assets/Photofunky.gif"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        Clock Time = new Clock();

        mainCharacter = new Character(30, 30, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W);
        mainCharacter2 = new Character2(Platform.WIDTH - 30, 30, 0, 96, KeyCode.LEFT, KeyCode.RIGHT, KeyCode.UP);
        buildEnemyList();
        /*  characterList.add(new Character(Platform.WIDTH-60, 30,0,96, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP));*/

        stageList = new ArrayList<>();
        stageList.add(new Stage(325, 240));

        score = new Score(50, HEIGHT - 370);
        score2 = new Score2(720, HEIGHT - 370);

        getChildren().add(backgroundImg);
        getChildren().addAll(stageList);
        getChildren().add(mainCharacter);
        getChildren().add(mainCharacter2);
        getChildren().addAll(score);
        getChildren().addAll(score2);
        getChildren().addAll(enemyList);
        getChildren().addAll(Time);
    }

    private void buildEnemyList() {
        for (int i = 0; i < 5; i++) {
            Enemy enemy = new Enemy(mainCharacter);
            enemyList.add(enemy);
        }
    }

    public Character getMainCharacter() {
        return mainCharacter;
    }

    public Character2 getMainCharacter2() {
        return mainCharacter2;
    }

    public ArrayList<Enemy> getEnemyList() {
        return enemyList;
    }

    public Keys getKeys() {
        return keys;
    }

    public Score getScore() {
        return score;
    }

    public Score2 getScore2() {
        return score2;
    }

    public void setScoreList(ArrayList<Score> scoreList) {
        this.scoreList = scoreList;
    }

    public void setMenu() {

        platformImg2 = new Image(getClass().getResourceAsStream("/platformer/assets/menubg.jpg"), WIDTH, HEIGHT, false, false);
        backgroundImg2 = new ImageView(platformImg2);

        backgroundImg2.setFitHeight(HEIGHT);
        backgroundImg2.setFitWidth(WIDTH);

        startBtn = new Button();
        startBtn.setLayoutX(400);
        startBtn.setLayoutY(300);
        startBtn.setMinSize(143, 55);
        startBtn.setMaxSize(143, 55);
//        startBtn.setStyle("-fx-background-image: url(/platformer/assets/start.png);-fx-background-size: 150px 62.5px; -fx-background-repeat: no-repeat; -fx-background-position: center;");
        startBtn.setGraphic(new ImageView(startPic));

        exitBtn = new Button();
        exitBtn.setLayoutX(600);
        exitBtn.setLayoutY(300);
        exitBtn.setMinSize(143, 55);
        exitBtn.setMaxSize(143, 55);
        exitBtn.setGraphic(new ImageView(exitPic));

        getChildren().addAll(backgroundImg2);
        getChildren().add(startBtn);
        getChildren().add(exitBtn);

    }

    public void runIntro() {

        player = new MediaPlayer(new Media(getClass().getResource("/platformer/assets/intro1.mp4").toExternalForm()));
        mediaView = new MediaView(player);

        mediaView.setFitHeight(HEIGHT);
        mediaView.setFitWidth(WIDTH);

        getChildren().addAll(mediaView);
        player.play();
    }

    public void setOver() {
        Image a = new Image(getClass().getResourceAsStream("/platformer/assets/whitebg.jpg"), WIDTH, HEIGHT, false, false);
        ImageView b = new ImageView(a);
        b.setOpacity(0.4);

        b.setFitHeight(HEIGHT);
        b.setFitWidth(WIDTH);

        Image gameOver = new Image(getClass().getResourceAsStream("/platformer/assets/gameover.png"));
        ImageView c = new ImageView(gameOver);
        c.setLayoutX(400);
        c.setLayoutY(100);

        retry = new Button();
        retry.setLayoutX(400);
        retry.setLayoutY(300);
        retry.setGraphic(new ImageView(startPic));

        exit2 = new Button();
        exit2.setLayoutX(400);
        exit2.setLayoutY(450);
        exit2.setGraphic(new ImageView(exitPic));

        getChildren().addAll(b);
        getChildren().addAll(c);
        getChildren().add(retry);
        getChildren().add(exit2);
    }

}

